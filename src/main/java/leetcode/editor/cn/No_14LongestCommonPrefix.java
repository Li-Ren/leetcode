//编写一个函数来查找字符串数组中的最长公共前缀。 
//
// 如果不存在公共前缀，返回空字符串 ""。 
//
// 示例 1: 
//
// 输入: ["flower","flow","flight"]
//输出: "fl"
// 
//
// 示例 2: 
//
// 输入: ["dog","racecar","car"]
//输出: ""
//解释: 输入不存在公共前缀。
// 
//
// 说明: 
//
// 所有输入只包含小写字母 a-z 。 
// Related Topics 字符串 
// 👍 1206 👎 0


package leetcode.editor.cn;

// java:最长公共前缀
public class No_14LongestCommonPrefix {

    public static void main(String[] args) {
        Solution solution = new No_14LongestCommonPrefix().new Solution();
        // TO TEST
        System.out.println(solution.longestCommonPrefix(new String[]{"flower", "flow", "flight"}));
    }

    class Solution {
        public String longestCommonPrefix(String[] strs) {
            //	{"flower", "flow", "flight"};
            //	先将第一个非空的作为基准。
            if (strs == null || strs.length == 0) {
                return "";
            }
            //	依次与第一位 比较 每个字符的第一位
            int length = strs[0].length();
            int count = strs.length;
            for (int i = 0; i < length; i++) {
                char c = strs[0].charAt(i);
                for (int j = 1; j < count; j++) {
                    if (i == strs[j].length() || strs[j].charAt(i) != c) {
                        return strs[0].substring(0, i);
                    }
                }
            }
            return strs[0];
        }
    }

}