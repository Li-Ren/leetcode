//判断一个整数是否是回文数。回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。 
//
// 示例 1: 
//
// 输入: 121
//输出: true
// 
//
// 示例 2: 
//
// 输入: -121
//输出: false
//解释: 从左向右读, 为 -121 。 从右向左读, 为 121- 。因此它不是一个回文数。
// 
//
// 示例 3: 
//
// 输入: 10
//输出: false
//解释: 从右向左读, 为 01 。因此它不是一个回文数。
// 
//
// 进阶: 
//
// 你能不将整数转为字符串来解决这个问题吗？ 
// Related Topics 数学 
// 👍 1171 👎 0


package leetcode.editor.cn;

// java:回文数
public class No_9PalindromeNumber {
    public static void main(String[] args) {
        Solution solution = new No_9PalindromeNumber().new Solution();
    }

    //leetcode submit region begin(Prohibit modification and deletion)
    class Solution {
        public boolean isPalindrome(int x) {
            // 特殊情况：
            // 如上所述，当 x < 0 时，x 不是回文数。
            // 同样地，如果数字的最后一位是 0，为了使该数字为回文，
            // 则其第一位数字也应该是 0
            // 只有 0 满足这一属性
            if (x < 0 || (x != 0 && x % 10 == 0)) {
                return false;
            }
            //  3201023
            int medile = 0;
            while (x > medile) {
                medile = medile * 10 + x % 10;
                x /= 10;
            }
            //  如果是偶数的话，则 直接  x == medile;
            //  如果是奇数的话，则 直接  x == medile / 10;
            return x == medile || x == medile / 10;
        }
    }
//leetcode submit region end(Prohibit modification and deletion)

}